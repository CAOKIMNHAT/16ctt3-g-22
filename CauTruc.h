﻿#include <stdio.h>

// Số lượng đơn thức tối đa trong đa thức.
#define SZ_MAX 50

/* --- */

// Cấu trúc cho đơn thức.
typedef struct
{
	double a; // Hệ số.
	char x; // Biến số.
	unsigned int n; // Số mũ.
} DonThuc;

// Cấu trúc cho đa thức.
typedef struct
{
	unsigned int N; // Số đơn thức hiện có trong đa thức.
	DonThuc N[SZ_MAX]; // Đa thức có tối đa `SZ_MAX` đơn thức.
} DaThuc;

/* --- */

// Nhập đa thức từ file txt.
void docFile(char *TenFile, DaThuc *DT);

// In đa thức ra file txt.
void xuatFile(char *TenFile, DaThuc DT);

/* --- */

// Rút gọn đa thức.
void RutGon(DaThuc *DT);

// Chuẩn hóa đa thức.
void ChuanHoa(DaThuc *DT);

// Cộng 2 đa thức.
DaThuc *CongDaThuc(DaThuc DT1, DaThuc DT2);

// Trừ 2 đa thức.
DaThuc *TruDaThuc(DaThuc DT1, DaThuc DT2);

// Nhân 2 đa thức.
DaThuc *NhanDaThuc(DaThuc DT1, DaThuc DT2);